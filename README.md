# RaytracerGLSL

[OpenGL](https://ja.wikipedia.org/wiki/OpenGL) の [Compute Shader](https://www.khronos.org/opengl/wiki/Compute_Shader) を用いて **パスレイトレーシング([Path-tracing](https://en.wikipedia.org/wiki/Path_tracing))** を実装する方法。金属や水面のフレネル反射や屈折を表現。[環境マッピング](https://ja.wikipedia.org/wiki/環境マッピング)には [HDRI](https://ja.wikipedia.org/wiki/ハイダイナミックレンジイメージ) を用い、簡易な[トーンマッピング](https://en.wikipedia.org/wiki/Tone_mapping)も導入。

![](https://github.com/LUXOPHIA/PathTracerGLSL/raw/master/--------/_SCREENSHOT/PathTracerGLSL.png)

### Path-tracing with Compute Shader
ソースコードは以下を参照。
> https://github.com/LUXOPHIA/PathTracerGLSL/blob/master/_DATA/Comput.glsl

----

[![Delphi Starter](http://img.en25.com/EloquaImages/clients/Embarcadero/%7B063f1eec-64a6-4c19-840f-9b59d407c914%7D_dx-starter-bn159.png)](https://www.embarcadero.com/jp/products/delphi/starter)
